FROM centos/nginx-116-centos7

ENV NAME="MkDocs" \
    MKDOCS_VERSION="1.1" \
    MATERIAL_VERSION="4.6.3" \
    SUMMARY="Platform for building and running MkDocs static sites." \
    DESCRIPTION="MkDocs is a fast, simple and downright gorgeous \
static site generator that's geared towards building project documentation. \
Documentation source files are written in Markdown, \
and configured with a single YAML configuration file." \
    # <https://click.palletsprojects.com/en/7.x/python3/>.
    LC_ALL="en_US.utf-8" \
    LANG="en_US.utf-8" \
    VIRTUAL_ENV="/opt/app-root/venv"

LABEL maintainer="CERN Authoring <authoring@cern.ch>" \
      summary="${SUMMARY}" \
      description="${DESCRIPTION}" \
      io.k8s.description="${DESCRIPTION}" \
      io.k8s.display-name="MkDocs ${MKDOCS_VERSION}" \
      io.openshift.tags="builder,${NAME}" \
      name="gitlab-registry.cern.ch/authoring/documentation/s2i-mkdocs-container" \
      version="${MKDOCS_VERSION}" \
      help="For more information visit https://cern.ch/authoring/" \
      usage="s2i build <SOURCE-REPOSITORY> gitlab-registry.cern.ch/authoring/documentation/s2i-mkdocs-container <APP-NAME>"

# Switch to the root user to install the necessary packages.
USER root

# Install the latest available Python and pip for CentOS 7
# as well as the specified version of MkDocs and the Material theme.
# Install VCS tools to support https://pip.readthedocs.io/en/1.1/requirements.html#requirements-file-format
RUN yum install -y rh-python36-python-pip git svn && \
    yum clean all

# Move the original assemble script to another file
# and copy our custom assemble script.
RUN mv /usr/libexec/s2i/assemble /usr/libexec/s2i/assemble-original
COPY assemble /usr/libexec/s2i/

# Switch back to the default user.
USER 1001

RUN /opt/rh/rh-python36/root/bin/python -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip install --upgrade pip setuptools wheel && \
    pip install mkdocs==${MKDOCS_VERSION} mkdocs-material==${MATERIAL_VERSION}
